Integrate virtualcitySTORY into existing MAP

- copy story folder + items.json in templates folder

- add to index.html
  <link rel="stylesheet" href="templates/story/story.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"
                  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                  crossorigin="anonymous"></script>
  <script src="templates/story/virtualcitystory.js"></script>
  
                
               
- add HTML Code to index.html (remove existing html elements)

<header class="tour-header vcm-header-height vcm-header-base vcm-border-splash">
      <div id="header">
          <div class="logo-box vcm_copyright_headerLogoBox">
              <div class="company-logo"></div>
          </div>
          <div class="title-box vcm_copyright_headerTitle"></div>
          <div class="tool-box" style="cursor:pointer"></div>
      </div>
  </header>
  <div id="story-frame" class="startscreen vcm-map-top">
      <!-- Content Start Screen -->
      <div class="balloon teaser-balloon" id="balloon-startscreen">
          <div class="balloon-content">
              <h1 class="balloon-title i18n_balloon_startscreen_title">
                  Willkommen<br>zur interaktiven<br>Storytelling Demo Tour
              </h1>
              <button id="tourstart-btn" class="buttonset one"><span class="i18n_balloon_startscreen_btn">Start</span>
              </button>
          </div>
          <div class="dialog-image-balloon"></div>
          <div class="balloon-left-edge"></div>
      </div>
      <!-- End of: Content Start Screen -->
      <!-- Content Tour -->
      <div id="tour-frame" style="display: none; /*overflow: hidden;*/">
          <div class="tour-top-navi">
              <a href="#demo-viewpoint" class="top-nav buttonset one"><i class="fa fa-globe"></i> Viewpoint</a>
              <a href="#demo-hiding" class="top-nav buttonset one"><i class="fa fa-eye-slash"></i> Hiding</a>
              <a href="#demo-highlight" class="top-nav buttonset one"><i class="fa fa-building"></i> Higlighting</a>
              <a href="#demo-planning" class="top-nav buttonset one"><i class="fa fa-building"></i> Planning</a>
              <a href="#demo-layer" class="top-nav buttonset one"><i class="fa fa-exchange"></i> Layer Switch</a>
              <a href="#demo-labels" class="top-nav buttonset one"><i class="fa fa-tag"></i> Labels</a>
              <a href="#demo-poi" class="top-nav buttonset one"><i class="fa fa-map-marker"></i> POI</a>
              <a href="#demo-rotate" class="top-nav buttonset one"><i class="fa fa-repeat"></i> Rotate</a>
              <a href="#story_start" class="top-nav buttonset one"><i class="fa fa-arrow-up"></i></a>

          </div>

          <div id="story-box" class="ui-scroll-content">
              <!--div id="story-scroll"-->
              <!-- <div id="story-scroll" class="scroll-container"> -->
              <div id="story_start" class="story-content">
                  <h1>virtualcitySYSTEMS</h1>
                  <h2>Storytelling starts here</h2>
                  <p><span>ScrollInteraction : Jump To Viewpoint</span></p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo Viewpoint -->
              <div id="demo-viewpoint" class="story-content">
                  <h2>Jump to viewpoint</h2>
                  <p><p><span>ScrollInteraction: Jump To Viewpoint</span></p></p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669" class="scale"><br>
                  </p>
                  <p><p><span>ClickInteractions: Interaction Chaining, jump to three viewpoints </span></p></p>
                  <p class="button-line">
                      <button id="demo-viewpointbutton" class="story-btn buttonset one">
                          <i class="fa fa-landmark"></i>
                          <span class="i18n_btn_science">Viewpoint Chaining on ButtonClick</span>
                      </button>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo hide buildings -->
              <div id="demo-hiding" class="story-content">
                  <h2>Hiding Buildings</h2>
                  <p>text text text</p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669" class="scale"><br>
                  </p>
                  <p class="button-line">
                      <button class="story-btn buttonset one" id="hideMyBuildingsButton">
                          <i class="far fa-eye-slash"></i>
                          <span>Hide my Buildings</span>
                      </button>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo highlight Builings -->
              <div id="demo-highlight" class="story-content">
                  <h2>Highlight Buildings</h2>
                  <p>text text text</p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669"
                           class="scale"><br>
                  </p>
                  <p class="button-line">
                      <button class="story-btn buttonset two" id="highlightMyBuildingButton">
                          <i class="far fa-building"></i>
                          <span class="i18n_btn_science">Highlight my Buildings</span>
                      </button>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo switch Layer -->
              <div id="demo-layer" class="story-content">
                  <h2>Show my Layer</h2>
                  <p>text text text</p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669" class="scale"><br>
                  </p>
                  <p class="button-line">
                      <button class="story-btn buttonset one" id="hidActivateLayerButton">
                          <i class="fa fa-exchange"></i>
                          <span>buildings</span>
                      </button>
                      <button class="story-btn buttonset one" id="hidActivateLayerButton2">
                          <i class="fa fa-exchange"></i>
                          <span>lehrpfad</span>
                      </button>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo show Planning -->
              <div id="demo-planning" class="story-content">
                  <h2>Show my Plannings</h2>
                  <p>text text text</p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669"
                           class="scale"><br>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!-- Demo Rotate -->
              <div id="demo-rotate" class="story-content">
                  <h2>Rotate around a viewpoint</h2>
                  <p>text text text</p>
                  <p>
                      <img src="images/backgrd.png" width="850" height="669"
                           class="scale"><br>
                  </p>
                  <p class="button-line" id="rotateButton">
                      <button class="story-btn  buttonset one">
                          <i class="fa fa-repeat"></i>
                          <span>Rotate</span>
                      </button>
                  </p>
                  <div style="height:100px"></div>
                  <div class="divider"></div>
              </div>
              <!--/div-->
          </div>
      </div>
      <div id="tour-button" >
          <i class="fa"></i>
      </div>
      <!-- End of: Content Tour -->
  </div>

  <div id="vcs_map_container"></div>
